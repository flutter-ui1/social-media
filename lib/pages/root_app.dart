import 'package:flutter/material.dart';
import 'package:social_media/theme/colors.dart';
import 'dart:math' as math;

import 'chat_page.dart';
import 'home_page.dart';
import 'profile_page.dart';
import 'saved_page.dart';

class RootApp extends StatefulWidget {
  const RootApp({Key? key}) : super(key: key);

  @override
  State<RootApp> createState() => _RootAppState();
}

class _RootAppState extends State<RootApp> {
  int activeTab = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: getFooter(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: getFloatingButton(),
      body: getBody(),
    );
  }

  Widget getFooter() {
    return Container(
      width: double.infinity,
      height: 65,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: grey.withOpacity(0.1),
            spreadRadius: 2,
            blurRadius: 20,
          )
        ],
        color: white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: (() => setState(() {
                    activeTab = 0;
                  })),
              child: Icon(
                Icons.home,
                size: 25,
                color: activeTab == 0 ? primary : black,
              ),
            ),
            SizedBox(
              width: 20,
            ),
            GestureDetector(
              onTap: (() => setState(() {
                    activeTab = 1;
                  })),
              child: Icon(
                Icons.chat_bubble_outline,
                size: 25,
                color: activeTab == 1 ? primary : black,
              ),
            ),
            SizedBox(
              width: 100,
            ),
            GestureDetector(
              onTap: (() => setState(() {
                    activeTab = 3;
                  })),
              child: Icon(
                Icons.favorite_border_outlined,
                size: 25,
                color: activeTab == 3 ? primary : black,
              ),
            ),
            SizedBox(
              width: 20,
            ),
            GestureDetector(
              onTap: (() => setState(() {
                    activeTab = 4;
                  })),
              child: Icon(
                Icons.account_circle,
                size: 25,
                color: activeTab == 4 ? primary : black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getFloatingButton() {
    return GestureDetector(
      onTap: () {
        setState(() {
          activeTab = 2;
        });
      },
      child: Transform.rotate(
        angle: math.pi / 4,
        child: Container(
          width: 60,
          height: 60,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: grey.withOpacity(0.1),
                spreadRadius: 2,
                blurRadius: 15,
                offset: Offset(0, 1),
              ),
            ],
            color: black,
            borderRadius: BorderRadius.circular(23),
          ),
          child: Transform.rotate(
            angle: math.pi / 4,
            child: Center(
                child: Icon(
              Icons.add_circle_outline,
              color: white,
              size: 26,
            )),
          ),
        ),
      ),
    );
  }

  Widget getBody() {
    return IndexedStack(
      index: activeTab,
      children: [
        HomePage(),
        ChatPage(),
        Center(
          child: Text('Upload'),
        ),
        SavedPage(),
        ProfilePage()
      ],
    );
  }
}
